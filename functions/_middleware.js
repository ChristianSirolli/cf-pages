import cloudflareAccessPlugin from "@cloudflare/pages-plugin-cloudflare-access";

export const onRequest = cloudflareAccessPlugin({
    domain: "https://sirolli.cloudflareaccess.com",
    aud: "b4384dc59025ac2dbfe93c87da9b450739690df070c0517b9f4b191cea3d0888",
  });